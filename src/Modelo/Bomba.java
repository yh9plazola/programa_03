package Modelo;

public class Bomba {
    private int numBomba;
    private Gasolina gasolina;
    private float capacidad;
    private float acumulador;
    
    public Bomba(){
        this.numBomba = 0;
        this.gasolina = null;
        this.capacidad = 0.000f;
        this.acumulador = 0.000f;
    }

    public Bomba(int numBomba, Gasolina gasolina, float capacidad, float acumulador) {
        this.numBomba = numBomba;
        this.gasolina = gasolina;
        this.capacidad = capacidad;
        this.acumulador = acumulador;
    }
    
    public Bomba(Bomba obj){
        this.numBomba = obj.numBomba;
        this.gasolina = obj.gasolina;
        this.capacidad = obj.capacidad;
        this.acumulador = obj.acumulador;
    }

    public int getNumBomba() {
        return this.numBomba;
    }

    public void setNumBomba(int numBomba) {
        this.numBomba = numBomba;
    }

    public Gasolina getGasolina() {
        return this.gasolina;
    }

    public void setGasolina(Gasolina gasolina) {
        this.gasolina = gasolina;
    }

    public float getCapacidad() {
        return this.capacidad;
    }

    public void setCapacidad(float capacidad) {
        this.capacidad = capacidad;
    }

    public float getAcumulador() {
        return this.acumulador;
    }

    public void setAcumulador(float acumulador) {
        this.acumulador = acumulador;
    }
    
    public void iniciarBomba(int numBomba, Gasolina gasolina, float capacidad, float acumulador){
        this.numBomba = numBomba;
        this.gasolina = gasolina;
        this.capacidad = capacidad;
        this.acumulador = acumulador;
    }

    public float inventarioGasolina(){
        return this.capacidad - this.acumulador;
    }
    
    public float venderGasolina(float cantidad){
        if(this.inventarioGasolina() < cantidad)
            return 0.0f;
        this.acumulador += cantidad;
        return cantidad * this.gasolina.getPrecio();
    }
    
    public float ventasTotales(){
        return this.acumulador * this.gasolina.getPrecio();
    }
}