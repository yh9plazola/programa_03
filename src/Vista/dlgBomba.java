/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package Vista;
/**
 *
 * @author yh9pl
 */
public class dlgBomba extends javax.swing.JDialog {
    public dlgBomba(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        pnlCapacidad = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        lblCapacidad = new javax.swing.JLabel();
        jSCapacidad = new javax.swing.JSlider();
        btnIniciarBomba = new javax.swing.JButton();
        pnlGasolina = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        cmbTipo = new javax.swing.JComboBox<>();
        pnlTipo = new javax.swing.JPanel();
        txtPrecio1 = new javax.swing.JTextField();
        lblTipo = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        th7 = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        th8 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        th9 = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        th4 = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        th5 = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        th6 = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        ts1 = new javax.swing.JLabel();
        jPanel15 = new javax.swing.JPanel();
        nd2 = new javax.swing.JLabel();
        jPanel16 = new javax.swing.JPanel();
        rd3 = new javax.swing.JLabel();
        jPanel20 = new javax.swing.JPanel();
        th0 = new javax.swing.JLabel();
        jPanel21 = new javax.swing.JPanel();
        point = new javax.swing.JLabel();
        jPanel22 = new javax.swing.JPanel();
        thzero = new javax.swing.JLabel();
        jPanel17 = new javax.swing.JPanel();
        rgs = new javax.swing.JLabel();
        jPanel18 = new javax.swing.JPanel();
        bcksp = new javax.swing.JLabel();
        txtNumeroBomba = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        pnlContPrecioLitros = new javax.swing.JPanel();
        pnlPrecioLitros = new javax.swing.JPanel();
        txtPrecio2 = new javax.swing.JTextField();
        txtLitros = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        pnlRVenta = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        pnlCostoTVentas = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        lblCosto = new javax.swing.JLabel();
        lblTotalVentas = new javax.swing.JLabel();
        pnlRegistrar = new javax.swing.JPanel();
        txtCantidad = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        btnRegistrar = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        txtContadorVentas = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });
        getContentPane().setLayout(null);

        jPanel1.setBackground(new java.awt.Color(36, 36, 36));
        jPanel1.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 30)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Vista/assets/fuego.png"))); // NOI18N
        jLabel1.setText("Gas de México");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(10, 10, 240, 41);

        jLabel2.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Número");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(20, 60, 70, 20);

        pnlCapacidad.setBackground(new java.awt.Color(51, 51, 51));
        pnlCapacidad.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlCapacidad.setLayout(null);

        jLabel4.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Capacidad");
        pnlCapacidad.add(jLabel4);
        jLabel4.setBounds(11, 10, 100, 20);

        lblCapacidad.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        lblCapacidad.setForeground(new java.awt.Color(255, 255, 255));
        lblCapacidad.setText("0Lts.");
        pnlCapacidad.add(lblCapacidad);
        lblCapacidad.setBounds(32, 224, 50, 16);

        jSCapacidad.setBackground(new java.awt.Color(51, 51, 51));
        jSCapacidad.setMaximum(200);
        jSCapacidad.setOrientation(javax.swing.JSlider.VERTICAL);
        jSCapacidad.setPaintLabels(true);
        jSCapacidad.setPaintTicks(true);
        jSCapacidad.setValue(0);
        jSCapacidad.setNextFocusableComponent(btnIniciarBomba);
        pnlCapacidad.add(jSCapacidad);
        jSCapacidad.setBounds(31, 40, 40, 180);

        jPanel1.add(pnlCapacidad);
        pnlCapacidad.setBounds(330, 0, 93, 247);

        btnIniciarBomba.setBackground(new java.awt.Color(255, 255, 0));
        btnIniciarBomba.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnIniciarBomba.setText("Iniciar Bomba");
        btnIniciarBomba.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnIniciarBomba.setNextFocusableComponent(txtCantidad);
        jPanel1.add(btnIniciarBomba);
        btnIniciarBomba.setBounds(211, 200, 110, 45);

        pnlGasolina.setBackground(new java.awt.Color(41, 41, 41));
        pnlGasolina.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlGasolina.setLayout(null);

        jLabel3.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Tipo de Gasolina");
        pnlGasolina.add(jLabel3);
        jLabel3.setBounds(10, 6, 120, 20);

        cmbTipo.setBackground(new java.awt.Color(0, 204, 102));
        cmbTipo.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        cmbTipo.setForeground(new java.awt.Color(255, 255, 255));
        cmbTipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Magna", "Premium", "Diesel" }));
        cmbTipo.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cmbTipo.setNextFocusableComponent(jSCapacidad);
        pnlGasolina.add(cmbTipo);
        cmbTipo.setBounds(10, 35, 120, 26);

        pnlTipo.setBackground(new java.awt.Color(0, 204, 102));
        pnlTipo.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        pnlTipo.setLayout(null);

        txtPrecio1.setEditable(false);
        txtPrecio1.setBackground(new java.awt.Color(170, 156, 107));
        txtPrecio1.setFont(new java.awt.Font("Segoe UI", 1, 16)); // NOI18N
        txtPrecio1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtPrecio1.setText("22.17");
        txtPrecio1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        txtPrecio1.setFocusable(false);
        pnlTipo.add(txtPrecio1);
        txtPrecio1.setBounds(7, 6, 50, 30);

        lblTipo.setFont(new java.awt.Font("Segoe UI", 1, 9)); // NOI18N
        lblTipo.setForeground(new java.awt.Color(255, 255, 255));
        lblTipo.setText("Magna");
        pnlTipo.add(lblTipo);
        lblTipo.setBounds(8, 35, 50, 13);

        pnlGasolina.add(pnlTipo);
        pnlTipo.setBounds(10, 70, 64, 50);

        jPanel8.setBackground(new java.awt.Color(0, 102, 102));
        jPanel8.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(0, 117, 117), new java.awt.Color(0, 117, 117), null, null));
        jPanel8.setLayout(null);

        th7.setFont(new java.awt.Font("Segoe UI", 0, 8)); // NOI18N
        th7.setForeground(new java.awt.Color(255, 255, 255));
        th7.setText("7");
        jPanel8.add(th7);
        th7.setBounds(2, -1, 20, 11);

        pnlGasolina.add(jPanel8);
        jPanel8.setBounds(90, 70, 10, 10);

        jPanel9.setBackground(new java.awt.Color(0, 102, 102));
        jPanel9.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(0, 117, 117), new java.awt.Color(0, 117, 117), null, null));
        jPanel9.setLayout(null);

        th8.setFont(new java.awt.Font("Segoe UI", 0, 8)); // NOI18N
        th8.setForeground(new java.awt.Color(255, 255, 255));
        th8.setText("8");
        jPanel9.add(th8);
        th8.setBounds(2, -1, 20, 11);

        pnlGasolina.add(jPanel9);
        jPanel9.setBounds(102, 70, 10, 10);

        jPanel10.setBackground(new java.awt.Color(0, 102, 102));
        jPanel10.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(0, 117, 117), new java.awt.Color(0, 117, 117), null, null));
        jPanel10.setLayout(null);

        th9.setFont(new java.awt.Font("Segoe UI", 0, 8)); // NOI18N
        th9.setForeground(new java.awt.Color(255, 255, 255));
        th9.setText("9");
        jPanel10.add(th9);
        th9.setBounds(2, -1, 20, 11);

        pnlGasolina.add(jPanel10);
        jPanel10.setBounds(114, 70, 10, 10);

        jPanel11.setBackground(new java.awt.Color(0, 102, 102));
        jPanel11.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(0, 117, 117), new java.awt.Color(0, 117, 117), null, null));
        jPanel11.setLayout(null);

        th4.setFont(new java.awt.Font("Segoe UI", 0, 8)); // NOI18N
        th4.setForeground(new java.awt.Color(255, 255, 255));
        th4.setText("4");
        jPanel11.add(th4);
        th4.setBounds(2, -1, 20, 11);

        pnlGasolina.add(jPanel11);
        jPanel11.setBounds(90, 82, 10, 10);

        jPanel12.setBackground(new java.awt.Color(0, 102, 102));
        jPanel12.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(0, 117, 117), new java.awt.Color(0, 117, 117), null, null));
        jPanel12.setLayout(null);

        th5.setFont(new java.awt.Font("Segoe UI", 0, 8)); // NOI18N
        th5.setForeground(new java.awt.Color(255, 255, 255));
        th5.setText("5");
        jPanel12.add(th5);
        th5.setBounds(2, -1, 20, 11);

        pnlGasolina.add(jPanel12);
        jPanel12.setBounds(102, 82, 10, 10);

        jPanel13.setBackground(new java.awt.Color(0, 102, 102));
        jPanel13.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(0, 117, 117), new java.awt.Color(0, 117, 117), null, null));
        jPanel13.setLayout(null);

        th6.setFont(new java.awt.Font("Segoe UI", 0, 8)); // NOI18N
        th6.setForeground(new java.awt.Color(255, 255, 255));
        th6.setText("6");
        th6.setToolTipText("");
        jPanel13.add(th6);
        th6.setBounds(2, -1, 20, 11);

        pnlGasolina.add(jPanel13);
        jPanel13.setBounds(114, 82, 10, 10);

        jPanel14.setBackground(new java.awt.Color(0, 102, 102));
        jPanel14.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(0, 117, 117), new java.awt.Color(0, 117, 117), null, null));
        jPanel14.setLayout(null);

        ts1.setFont(new java.awt.Font("Segoe UI", 0, 8)); // NOI18N
        ts1.setForeground(new java.awt.Color(255, 255, 255));
        ts1.setText("1");
        jPanel14.add(ts1);
        ts1.setBounds(2, -1, 20, 11);

        pnlGasolina.add(jPanel14);
        jPanel14.setBounds(90, 94, 10, 10);

        jPanel15.setBackground(new java.awt.Color(0, 102, 102));
        jPanel15.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(0, 117, 117), new java.awt.Color(0, 117, 117), null, null));
        jPanel15.setLayout(null);

        nd2.setFont(new java.awt.Font("Segoe UI", 0, 8)); // NOI18N
        nd2.setForeground(new java.awt.Color(255, 255, 255));
        nd2.setText("2");
        jPanel15.add(nd2);
        nd2.setBounds(2, -1, 20, 11);

        pnlGasolina.add(jPanel15);
        jPanel15.setBounds(102, 94, 10, 10);

        jPanel16.setBackground(new java.awt.Color(0, 102, 102));
        jPanel16.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(0, 117, 117), new java.awt.Color(0, 117, 117), null, null));
        jPanel16.setLayout(null);

        rd3.setFont(new java.awt.Font("Segoe UI", 0, 8)); // NOI18N
        rd3.setForeground(new java.awt.Color(255, 255, 255));
        rd3.setText("3");
        jPanel16.add(rd3);
        rd3.setBounds(2, -1, 20, 11);

        pnlGasolina.add(jPanel16);
        jPanel16.setBounds(114, 94, 10, 10);

        jPanel20.setBackground(new java.awt.Color(0, 102, 102));
        jPanel20.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(0, 117, 117), new java.awt.Color(0, 117, 117), null, null));
        jPanel20.setLayout(null);

        th0.setFont(new java.awt.Font("Segoe UI", 0, 8)); // NOI18N
        th0.setForeground(new java.awt.Color(255, 255, 255));
        th0.setText("0");
        jPanel20.add(th0);
        th0.setBounds(2, -1, 20, 11);

        pnlGasolina.add(jPanel20);
        jPanel20.setBounds(90, 106, 10, 10);

        jPanel21.setBackground(new java.awt.Color(0, 102, 102));
        jPanel21.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(0, 117, 117), new java.awt.Color(0, 117, 117), null, null));
        jPanel21.setLayout(null);

        point.setFont(new java.awt.Font("Segoe UI", 0, 8)); // NOI18N
        point.setForeground(new java.awt.Color(255, 255, 255));
        point.setText(".");
        jPanel21.add(point);
        point.setBounds(4, -1, 20, 11);

        pnlGasolina.add(jPanel21);
        jPanel21.setBounds(102, 106, 10, 10);

        jPanel22.setBackground(new java.awt.Color(0, 102, 102));
        jPanel22.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(0, 117, 117), new java.awt.Color(0, 117, 117), null, null));
        jPanel22.setLayout(null);

        thzero.setFont(new java.awt.Font("Segoe UI", 0, 8)); // NOI18N
        thzero.setForeground(new java.awt.Color(255, 255, 255));
        thzero.setText("00");
        jPanel22.add(thzero);
        thzero.setBounds(0, -1, 20, 11);

        pnlGasolina.add(jPanel22);
        jPanel22.setBounds(114, 106, 10, 10);

        jPanel17.setBackground(new java.awt.Color(255, 204, 0));
        jPanel17.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel17.setLayout(null);

        rgs.setFont(new java.awt.Font("Segoe UI", 0, 8)); // NOI18N
        rgs.setText("+");
        jPanel17.add(rgs);
        rgs.setBounds(2, -1, 20, 11);

        pnlGasolina.add(jPanel17);
        jPanel17.setBounds(126, 82, 10, 10);

        jPanel18.setBackground(new java.awt.Color(255, 51, 51));
        jPanel18.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel18.setLayout(null);

        bcksp.setFont(new java.awt.Font("Segoe UI", 0, 8)); // NOI18N
        bcksp.setForeground(new java.awt.Color(255, 255, 255));
        bcksp.setText("<");
        bcksp.setToolTipText("");
        jPanel18.add(bcksp);
        bcksp.setBounds(2, -1, 20, 11);

        pnlGasolina.add(jPanel18);
        jPanel18.setBounds(126, 70, 10, 10);

        jPanel1.add(pnlGasolina);
        pnlGasolina.setBounds(10, 110, 190, 136);

        txtNumeroBomba.setBackground(new java.awt.Color(170, 156, 107));
        txtNumeroBomba.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txtNumeroBomba.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtNumeroBomba.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        txtNumeroBomba.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtNumeroBomba.setFocusCycleRoot(true);
        txtNumeroBomba.setNextFocusableComponent(cmbTipo);
        jPanel1.add(txtNumeroBomba);
        txtNumeroBomba.setBounds(100, 70, 50, 24);
        jPanel1.add(jLabel6);
        jLabel6.setBounds(10, 210, 0, 0);

        pnlContPrecioLitros.setBackground(new java.awt.Color(41, 41, 41));
        pnlContPrecioLitros.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.LOWERED, null, null, null, new java.awt.Color(0, 0, 0)));
        pnlContPrecioLitros.setLayout(null);

        pnlPrecioLitros.setBackground(new java.awt.Color(170, 156, 107));
        pnlPrecioLitros.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        pnlPrecioLitros.setLayout(null);

        txtPrecio2.setEditable(false);
        txtPrecio2.setBackground(new java.awt.Color(170, 156, 107));
        txtPrecio2.setFont(new java.awt.Font("Segoe UI", 1, 16)); // NOI18N
        txtPrecio2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtPrecio2.setBorder(null);
        txtPrecio2.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtPrecio2.setFocusable(false);
        pnlPrecioLitros.add(txtPrecio2);
        txtPrecio2.setBounds(6, 5, 81, 20);

        txtLitros.setEditable(false);
        txtLitros.setBackground(new java.awt.Color(170, 156, 107));
        txtLitros.setFont(new java.awt.Font("Segoe UI", 1, 16)); // NOI18N
        txtLitros.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtLitros.setBorder(null);
        txtLitros.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtLitros.setFocusable(false);
        pnlPrecioLitros.add(txtLitros);
        txtLitros.setBounds(5, 25, 81, 22);

        pnlContPrecioLitros.add(pnlPrecioLitros);
        pnlPrecioLitros.setBounds(10, 10, 90, 50);

        jPanel1.add(pnlContPrecioLitros);
        pnlContPrecioLitros.setBounds(210, 110, 110, 80);

        jLabel8.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("de Bomba:");
        jPanel1.add(jLabel8);
        jLabel8.setBounds(20, 76, 80, 20);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 440, 250);

        pnlRVenta.setBackground(new java.awt.Color(36, 36, 36));
        pnlRVenta.setLayout(null);

        jPanel3.setBackground(new java.awt.Color(36, 36, 36));
        jPanel3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        jPanel3.setLayout(null);

        jLabel9.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Realizar venta");
        jPanel3.add(jLabel9);
        jLabel9.setBounds(20, 10, 130, 25);

        pnlCostoTVentas.setBackground(new java.awt.Color(36, 36, 36));
        pnlCostoTVentas.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel12.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Costo: ");

        jLabel13.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Total de ventas");

        jLabel14.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText(": ");

        lblCosto.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        lblCosto.setForeground(new java.awt.Color(255, 255, 255));
        lblCosto.setText("$0.00");

        lblTotalVentas.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        lblTotalVentas.setForeground(new java.awt.Color(255, 255, 255));
        lblTotalVentas.setText("$0.00");

        javax.swing.GroupLayout pnlCostoTVentasLayout = new javax.swing.GroupLayout(pnlCostoTVentas);
        pnlCostoTVentas.setLayout(pnlCostoTVentasLayout);
        pnlCostoTVentasLayout.setHorizontalGroup(
            pnlCostoTVentasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCostoTVentasLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlCostoTVentasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlCostoTVentasLayout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblCosto))
                    .addComponent(jLabel13)
                    .addGroup(pnlCostoTVentasLayout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblTotalVentas)))
                .addContainerGap(68, Short.MAX_VALUE))
        );
        pnlCostoTVentasLayout.setVerticalGroup(
            pnlCostoTVentasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCostoTVentasLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlCostoTVentasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(lblCosto))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlCostoTVentasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(lblTotalVentas))
                .addContainerGap(12, Short.MAX_VALUE))
        );

        jPanel3.add(pnlCostoTVentas);
        pnlCostoTVentas.setBounds(210, 40, 180, 100);

        pnlRegistrar.setBackground(new java.awt.Color(36, 36, 36));
        pnlRegistrar.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        txtCantidad.setBackground(new java.awt.Color(170, 156, 107));
        txtCantidad.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txtCantidad.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtCantidad.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        txtCantidad.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtCantidad.setEnabled(false);
        txtCantidad.setNextFocusableComponent(btnRegistrar);

        jLabel11.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Cantidad:");

        btnRegistrar.setBackground(new java.awt.Color(204, 204, 204));
        btnRegistrar.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnRegistrar.setText("Registrar");
        btnRegistrar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnRegistrar.setEnabled(false);
        btnRegistrar.setNextFocusableComponent(cmbTipo);

        javax.swing.GroupLayout pnlRegistrarLayout = new javax.swing.GroupLayout(pnlRegistrar);
        pnlRegistrar.setLayout(pnlRegistrarLayout);
        pnlRegistrarLayout.setHorizontalGroup(
            pnlRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlRegistrarLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pnlRegistrarLayout.createSequentialGroup()
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(10, Short.MAX_VALUE))
        );
        pnlRegistrarLayout.setVerticalGroup(
            pnlRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlRegistrarLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11)
                    .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(btnRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(8, Short.MAX_VALUE))
        );

        jPanel3.add(pnlRegistrar);
        pnlRegistrar.setBounds(20, 40, 170, 100);

        jLabel5.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Ventas:");
        jPanel3.add(jLabel5);
        jLabel5.setBounds(284, 10, 60, 30);

        txtContadorVentas.setEditable(false);
        txtContadorVentas.setBackground(new java.awt.Color(170, 156, 107));
        txtContadorVentas.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        txtContadorVentas.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtContadorVentas.setText("0");
        txtContadorVentas.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        txtContadorVentas.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtContadorVentas.setFocusable(false);
        jPanel3.add(txtContadorVentas);
        txtContadorVentas.setBounds(339, 10, 50, 24);

        pnlRVenta.add(jPanel3);
        jPanel3.setBounds(10, 8, 414, 160);

        getContentPane().add(pnlRVenta);
        pnlRVenta.setBounds(0, 250, 440, 179);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        this.setVisible(false);
        this.dispose();
        System.exit(0);
    }//GEN-LAST:event_formWindowClosed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(dlgBomba.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(dlgBomba.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(dlgBomba.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(dlgBomba.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                dlgBomba dialog = new dlgBomba(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel bcksp;
    public javax.swing.JButton btnIniciarBomba;
    public javax.swing.JButton btnRegistrar;
    public javax.swing.JComboBox<String> cmbTipo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    public javax.swing.JSlider jSCapacidad;
    public javax.swing.JLabel lblCapacidad;
    public javax.swing.JLabel lblCosto;
    public javax.swing.JLabel lblTipo;
    public javax.swing.JLabel lblTotalVentas;
    private javax.swing.JLabel nd2;
    public javax.swing.JPanel pnlCapacidad;
    private javax.swing.JPanel pnlContPrecioLitros;
    public javax.swing.JPanel pnlCostoTVentas;
    public javax.swing.JPanel pnlGasolina;
    private javax.swing.JPanel pnlPrecioLitros;
    private javax.swing.JPanel pnlRVenta;
    public javax.swing.JPanel pnlRegistrar;
    public javax.swing.JPanel pnlTipo;
    private javax.swing.JLabel point;
    private javax.swing.JLabel rd3;
    private javax.swing.JLabel rgs;
    private javax.swing.JLabel th0;
    private javax.swing.JLabel th4;
    private javax.swing.JLabel th5;
    private javax.swing.JLabel th6;
    private javax.swing.JLabel th7;
    private javax.swing.JLabel th8;
    private javax.swing.JLabel th9;
    private javax.swing.JLabel thzero;
    private javax.swing.JLabel ts1;
    public javax.swing.JTextField txtCantidad;
    public javax.swing.JTextField txtContadorVentas;
    public javax.swing.JTextField txtLitros;
    public javax.swing.JTextField txtNumeroBomba;
    public javax.swing.JTextField txtPrecio1;
    public javax.swing.JTextField txtPrecio2;
    // End of variables declaration//GEN-END:variables
}
