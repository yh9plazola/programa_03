package Controlador;

import Modelo.Gasolina;
import Modelo.Bomba;
import Vista.dlgBomba;
import Vista.assets.FuentePersonalizada;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.event.DocumentEvent;

public class Controlador implements ActionListener, ChangeListener{

    private Bomba bomba;
    private dlgBomba vista;
    private Timer timer;
    private static int i;
    
    public Controlador(Bomba bomba, dlgBomba vista){
        this.bomba = bomba;
        this.vista = vista;
        // Escuchar los botones
        this.vista.btnIniciarBomba.addActionListener(this);
        this.vista.btnRegistrar.addActionListener(this);
        this.vista.cmbTipo.addActionListener(this);
        this.vista.jSCapacidad.addChangeListener(this);
    }
    
    public static void main(String[] args){
        Bomba bomba = new Bomba();
        dlgBomba vista = new dlgBomba(new JFrame(), true);
        Controlador controlador = new Controlador(bomba, vista);
        controlador.init();
    }
    
    private void init(){
        FuentePersonalizada font = new FuentePersonalizada();
        this.vista.setTitle(":: Maquina expendedora de Gasolina ::");
        this.vista.setSize(452, 468);
        this.vista.txtNumeroBomba.setFont(font.fuente(FuentePersonalizada.DIG, 1, 16));
        this.vista.txtContadorVentas.setFont(font.fuente(FuentePersonalizada.DIG, 1, 16));
        this.vista.txtPrecio1.setFont(font.fuente(FuentePersonalizada.DIG, 1, 16));
        this.vista.txtPrecio2.setFont(font.fuente(FuentePersonalizada.DIG, 1, 18));
        this.vista.txtLitros.setFont(font.fuente(FuentePersonalizada.DIG, 1, 18));
        this.vista.txtCantidad.setFont(font.fuente(FuentePersonalizada.DIG, 1, 16));
        this.vista.setLocationRelativeTo(null);
        this.vista.setVisible(true);
    }
    
    private void alternarComponentesHabilitados(Boolean bool){
        // Iniciar Bomba
        this.vista.txtNumeroBomba.setEnabled(!bool);
        this.vista.cmbTipo.setEnabled(!bool);
        this.vista.jSCapacidad.setEnabled(!bool);
        this.vista.btnIniciarBomba.setEnabled(!bool);
        this.vista.btnIniciarBomba.setBackground(new Color(204, 204, 204));
        this.vista.pnlGasolina.setBackground(new Color(36, 36, 36));
        this.vista.pnlCapacidad.setBackground(new Color(36, 36, 36));
        this.vista.jSCapacidad.setBackground(new Color(36, 36, 36));
        this.vista.txtPrecio2.setText("00.00");
        this.vista.txtLitros.setText("00.000");
        // Realizar Venta
        this.vista.pnlRegistrar.setBackground(new Color(41, 41, 41));
        this.vista.txtCantidad.setEnabled(bool);
        this.vista.btnRegistrar.setEnabled(bool);
        this.vista.btnRegistrar.setBackground(new Color(255,255,0));
        this.vista.pnlCostoTVentas.setBackground(new Color(41, 41, 41));
    }
    
    // Ejecutar la animación de los números del medidor
    private void ejecutarMedidor(float p, float l){
        if(Float.parseFloat(this.vista.txtPrecio2.getText()) < Float.parseFloat(this.vista.lblCosto.getText()) ||
              Float.parseFloat(this.vista.txtLitros.getText()) < Float.parseFloat(this.vista.txtCantidad.getText())){
            if(p < Float.parseFloat(this.vista.lblCosto.getText())){
                p += (float)Math.random() * 77.750 + 1;
            }
            if(l < Float.parseFloat(this.vista.txtCantidad.getText())){
                l += (float)Math.random() * 3.3;
            }
            this.vista.txtPrecio2.setText(String.format("%.2f", p));
            this.vista.txtLitros.setText(String.format("%.3f", l));
            final float precio = p;
            final float litros = l;
            if(this.timer != null)
                this.timer.stop();
            this.timer = new Timer(10, new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e) {
                    // Mientras el precio y litros no hayan llegado
                    // al los valores indicados seguimos en bucle
                    ejecutarMedidor(precio, litros);
                }
            });
            this.timer.start();
        }
        else{
            // Corregir valores que exeden a los indicados
            if(p > Float.parseFloat(this.vista.lblCosto.getText())){
                p = Float.parseFloat(String.format("%.2f", Float.parseFloat(this.vista.lblCosto.getText())));
            }
            if(l > Float.parseFloat(this.vista.txtCantidad.getText())){
                l = Float.parseFloat(String.format("%.3f", Float.parseFloat(this.vista.txtCantidad.getText())));
            }
            this.vista.txtPrecio2.setText(String.format("%.2f", p));
            this.vista.txtLitros.setText(String.format("%.3f", l));
            this.timer.stop();
            // Fin de recursividad
            // Seteamos el campo de cantidad y habilitamos el botón registrar
            // Incrementamos el total de ventas realizadas
            // Y añadimos el costo generado al total vendido
            if(this.vista.txtLitros.getText().equals(this.vista.txtCantidad.getText() + ".000")){
                JOptionPane.showMessageDialog(this.vista,"Se realizó la venta con exito.", ":: Maquina expendedora de Gasolina ::", JOptionPane.INFORMATION_MESSAGE);
                this.vista.txtPrecio2.setText("00.00");
                this.vista.txtLitros.setText("00.000");
                this.vista.txtCantidad.setText("");
                this.vista.txtCantidad.requestFocusInWindow();
                this.vista.btnRegistrar.setEnabled(true);
                this.vista.btnRegistrar.setBackground(new Color(255, 255, 0));
                this.vista.jSCapacidad.setValue((int)this.bomba.inventarioGasolina());
                this.vista.lblTotalVentas.setText(String.format("%.2f", this.bomba.ventasTotales()));
                Controlador.i++;
                this.vista.txtContadorVentas.setText(String.valueOf(Controlador.i));
                this.vista.txtCantidad.setEnabled(true);
                if(this.vista.jSCapacidad.getValue() <= 0){
                    this.vista.txtCantidad.setEnabled(false);
                    this.vista.btnRegistrar.setEnabled(false);
                    this.vista.btnRegistrar.setBackground(new Color(204, 204, 204));
                    JOptionPane.showMessageDialog(this.vista, "Ya no hay más gasolina para vender", ":: Máquina expendedora de gasolina ::", JOptionPane.WARNING_MESSAGE);
                }
            }
            return;
        }
    }
    
    private int isIniciarBombaIncompletoNegativo(){
        try{
            if(this.vista.txtNumeroBomba.getText().equals("") ||
                Integer.parseInt(this.vista.txtNumeroBomba.getText()) <= 0 ||
                this.vista.jSCapacidad.getValue() < 200){
                return 1;
            }
        }
        catch(NumberFormatException ex1){
            JOptionPane.showMessageDialog(this.vista, String.format("Ocurrio el siguiente error %s", ex1.getMessage()), ":: Máquina expendedora de gasolina ::", JOptionPane.ERROR_MESSAGE);
            return 2;
        }
        return 0;
    }
    
    private int isRegistrarVacioNegativo(){
        try{
            for(int x = 0; x < this.vista.txtCantidad.getText().length(); x++){
                if(this.vista.txtCantidad.getText().charAt(x) == '.'){
                    return 1;
                }
            }
            if(this.vista.txtCantidad.getText().equals("") ||
               Float.parseFloat(this.vista.txtCantidad.getText()) < 0.000f){
                return 1;
            }
        }
        catch(NumberFormatException ex2){
            JOptionPane.showMessageDialog(this.vista, String.format("Ocurrio el siguiente error %s", ex2.getMessage()), ":: Máquina expendedora de gasolina ::", JOptionPane.ERROR_MESSAGE);
            return 2;
        }
        return 0;
    }
    
    // Escuchar las acciones del usuario
    @Override
    public void actionPerformed(ActionEvent e) {
        // Establecer el color de acuerdo con el tipo de gasolina seleccionado
        if(e.getSource() == this.vista.cmbTipo){
            Color background = null;
            String texto = null;
            float precio = 0.0f;
            switch(this.vista.cmbTipo.getSelectedIndex()){
                case 0:
                    background = new Color(0,204,102);
                    texto = "Magna";
                    precio = 22.17f;
                    break;
                case 1:
                    background = new Color(213, 37, 46);
                    texto = "Premium";
                    precio = 23.98f;
                    break;
                case 2:
                    background = new Color(20, 20, 20);
                    texto = "Diesel";
                    precio = 24.71f;
                    break;
            }
            this.vista.cmbTipo.setBackground(background);
            this.vista.pnlTipo.setBackground(background);
            this.vista.lblTipo.setText(texto);
            this.vista.txtPrecio1.setText(String.valueOf(precio));
        }
        if(e.getSource() == this.vista.btnIniciarBomba){
            int var = this.isIniciarBombaIncompletoNegativo();
            if(var == 1){
                JOptionPane.showMessageDialog(this.vista, "Asegurate de proporcionar un número de bomba válido y tener al menos 200Lts. de capacidad inicial", ":: Máquina expendedora de gasolina ::", JOptionPane.WARNING_MESSAGE);
                return;
            }
            if(var == 2){
                return;
            }
            // Creamos e inicializamos la bomba
            this.bomba = new Bomba();
            this.bomba.iniciarBomba(Integer.parseInt(this.vista.txtNumeroBomba.getText()), 
                                    new Gasolina(this.vista.cmbTipo.getSelectedIndex(),
                                    this.vista.cmbTipo.getSelectedItem().toString(), 
                                    Float.parseFloat(this.vista.txtPrecio1.getText())), 
                                    this.vista.jSCapacidad.getValue(), 0.000f);
            
            // Actualizamos el costo de la venta a realizar escuchando al txtCantidad
            // Escuchamos los 3 casos de un JTextField
            this.vista.txtCantidad.getDocument().addDocumentListener(new DocumentListener(){
                @Override
                public void insertUpdate(DocumentEvent e) {
                    try{
                        float costo = Float.parseFloat(vista.txtCantidad.getText()) * bomba.getGasolina().getPrecio();
                        vista.lblCosto.setText(String.format("%.2f", costo));
                        for(int x = 0; x < vista.txtCantidad.getText().length(); x++){
                            if(vista.txtCantidad.getText().charAt(x) == '.'){
                                throw new NumberFormatException();
                            }
                        }
                    }
                    catch(NumberFormatException ex){
                        vista.lblCosto.setText("");
                    }
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    try{
                        float costo = Float.parseFloat(vista.txtCantidad.getText()) * bomba.getGasolina().getPrecio();
                        vista.lblCosto.setText(String.format("%.2f", costo));
                        for(int x = 0; x < vista.txtCantidad.getText().length(); x++){
                            if(vista.txtCantidad.getText().charAt(x) == '.'){
                                throw new NumberFormatException();
                            }
                        }
                    }
                    catch(NumberFormatException ex){
                        vista.lblCosto.setText("");
                    }
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    try{
                        float costo = Float.parseFloat(vista.txtCantidad.getText()) * bomba.getGasolina().getPrecio();
                        vista.lblCosto.setText(String.format("%.2f", costo));
                        for(int x = 0; x < vista.txtCantidad.getText().length(); x++){
                            if(vista.txtCantidad.getText().charAt(x) == '.'){
                                throw new NumberFormatException();
                            }
                        }
                    }
                    catch(NumberFormatException ex){
                        vista.lblCosto.setText("");
                    }
                }
            });
            this.alternarComponentesHabilitados(Boolean.TRUE);
        }
        if(e.getSource() == this.vista.btnRegistrar){
            int var = this.isRegistrarVacioNegativo();
            if(var == 1){
                JOptionPane.showMessageDialog(this.vista, "Asegurate de proporcionar una cantidad válida", ":: Máquina expendedora de gasolina ::", JOptionPane.WARNING_MESSAGE);
                return;
            }
            if(var == 2){
                return;
            }
            if(Float.parseFloat(this.vista.txtCantidad.getText()) > 0 &&
               Float.parseFloat(this.vista.txtCantidad.getText()) <= this.bomba.inventarioGasolina()){
                this.vista.lblCosto.setText(String.format("%.2f", this.bomba.venderGasolina(Float.parseFloat(this.vista.txtCantidad.getText()))));
                // Ejecutamos animación del medidor
                this.vista.btnRegistrar.setEnabled(false);
                this.vista.btnRegistrar.setBackground(new Color(204, 204, 204));
                this.vista.txtCantidad.setEnabled(false);
                this.ejecutarMedidor(0.00f, 0.000f);                
            }
        }
    }    
    // Escuchar al JSlider
    @Override
    public void stateChanged(ChangeEvent e) {
        if(e.getSource() == this.vista.jSCapacidad){
            this.vista.lblCapacidad.setText(String.valueOf(this.vista.jSCapacidad.getValue()) + "Lts.");
        }
    }
}
